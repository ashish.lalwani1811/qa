<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Question extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function owner(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function setTitleAttribute($title){
        $this -> attributes['title'] = $title;
        $this -> attributes['slug'] = Str::slug($title);
    }
}
